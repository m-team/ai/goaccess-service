#!/usr/bin/env bash
# vim:sw=2:ts=2:et

set -ueo pipefail
# DEBUG
# set -x

# start crond daemon
cron -l -L 8

export GOACCESS_ETC_PATH="${GOACCESS_ETC_PATH:-/opt/goaccess-etc}"    # directory with configuration files for goaccess
export GOACCESS_OUT_PATH="${GOACCESS_OUT_PATH:-/opt/goaccess-out}"    # directory to store goaccess outputs
export GOACCESS_WEB_ROUTE="${GOACCESS_WEB_ROUTE:-goaccess}"           # web route to access goaccess HTML report
GOACCESS_EXEC="${GOACCESS_EXEC:-/usr/local/bin/goaccess-exec.sh}"     # main script to execute goaccess web log analyser
GOACCESS_CRON="${GOACCESS_CRON:-$GOACCESS_ETC_PATH/goaccess.cron}"    # cronjob file

# store following environment settings for re-use
echo """
export GOACCESS_ETC_PATH=$GOACCESS_ETC_PATH
export GOACCESS_OUT_PATH=$GOACCESS_OUT_PATH
""" >> /etc/goaccess/goaccess-set.env

# if $GOACCESS_CRON file found, use it, otherwise set every 6 hours run
if [ -f $GOACCESS_CRON ]; then
  crontab $GOACCESS_CRON
else
  echo "5 */6 * * * $GOACCESS_EXEC" > /etc/cron.d/goaccess.cron
  crontab /etc/cron.d/goaccess.cron
fi

# create symbolic link ${GOACCESS_WEB_ROUTE} to default nginx location, where goaccess files are placed
if [ ! -e /var/www/${GOACCESS_WEB_ROUTE} ]; then
  mkdir -p /var/www
  ln -s /usr/share/nginx/html /var/www/${GOACCESS_WEB_ROUTE}
fi

# make first time execution
$GOACCESS_EXEC

# original entrypoint for nginx
exec /nginx-entrypoint.sh "$@"
