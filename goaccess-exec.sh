#!/usr/bin/env bash

# Main script to execute goaccess log analyser
# 1. Relevant variables are configured
# 2. Find access logs
# 3. Pre-process those access logs, if the pre-processing script exists
# 4. goaccess web analyser is called with either custom configuration file or with preset settings

# 1. Configure variables. can be re-defined via Environment settings
if [ -f /etc/goaccess/goaccess-set.env ]; then source /etc/goaccess/goaccess-set.env; fi
GOACCESS_ETC_PATH="${GOACCESS_ETC_PATH:-/opt/goaccess-etc}"                        # directory with configuration files for goaccess
GOACCESS_OUT_PATH="${GOACCESS_OUT_PATH:-/opt/goaccess-out}"                        # directory to store goaccess outputs
GOACCESS_CONF="${GOACCESS_CONF:-$GOACCESS_ETC_PATH/goaccess.conf}"                 # custom configuration file to use
GOACCESS_ACCESSLOG_PATH="${GOACCESS_ACCESSLOG_PATH:-/var/log/srv-access/}"         # directory to search for access logs
GOACCESS_ACCESSLOG_PATTERN="${GOACCESS_ACCESSLOG_PATTERN:-access*}"                # pattern to search for access logs
GOACCESS_ACCESSLOG_N="${GOACCESS_ACCESSLOG_N:-1}"                                  # number of access.log files to process
GOACCESS_SKIM_SCRIPT="${GOACCESS_SKIM_SCRIPT:-$GOACCESS_ETC_PATH/goaccess-skim.sh}" # define where skim script is located
GOACCESS_SKIMMED_PATH="${GOACCESS_SKIMMED_PATH:-$GOACCESS_OUT_PATH/skimmed/}"      # path to skimmed/filtered files
NGINX_HTML_PATH="/usr/share/nginx/html"                                            # default html location for nginx
GOACCESS_REPORT_HTML="${GOACCESS_REPORT_HTML:-$GOACCESS_OUT_PATH/report.html}"     # path for the final goaccess HTML report
GOACCESS_REPORT_JSON="${GOACCESS_REPORT_JSON:-$GOACCESS_OUT_PATH/report.json}"     # path for the final goaccess JSON report
## if GOACCESS_CONF is defined, the following variables are not taken into account:
GOACCESS_ACCESSLOG_FORMAT="${GOACCESS_ACCESSLOG_FORMAT:-COMBINED}"                 # access.log format
GOACCESS_KEEP_LAST="${GOACCESS_KEEP_LAST:-180}"                                    # for how many days keep in storage
# Database for IP to COUNTRY (options:
# https://db-ip.com; http://dev.maxmind.com/geoip/geoip2/geolite2/)
GOACCESS_DBIP="${GOACCESS_DBIP:-/opt/dbip/dbip-country-lite.mmdb}"                 # Database for IP to COUNTRY
GOACCESS_DB_PATH="${GOACCESS_DB_PATH:-$GOACCESS_OUT_PATH/db}"                      # goaccess DB for persistance

# 2. Get list of access log files, limited by GOACCESS_ACCESSLOG_N
log_files_all=($(ls -1 $(realpath ${GOACCESS_ACCESSLOG_PATH})/${GOACCESS_ACCESSLOG_PATTERN}))
log_files="${log_files_all[0]}"
for (( c=1; c<$GOACCESS_ACCESSLOG_N; c++ ))
do 
  log_files="$log_files ${log_files_all[c]}"
done
# default GOACCESS_INPUT_FILES is the list of raw access logs
GOACCESS_INPUT_FILES=$log_files

env
echo $GOACCESS_INPUT_FILES

# 3. Call pre-processing/skimming script
if [ -f $GOACCESS_SKIM_SCRIPT ]; then
  mkdir -p $GOACCESS_SKIMMED_PATH
  IFS=' ' read -r -a logs_array <<< "$log_files"
  input_files=""
  for input in "${logs_array[@]}"
  do
    output="$(realpath ${GOACCESS_SKIMMED_PATH})/$(basename $input).skim"
    $GOACCESS_SKIM_SCRIPT $input > $output
    [[ ${#input_files} == 0 ]] && input_files="$output" || input_files="$input_files $output"
  done
  GOACCESS_INPUT_FILES=$input_files
fi

# 4. Exec goaccess
if [ -f $GOACCESS_CONF ]; then
  goaccess $GOACCESS_INPUT_FILES \
-p $GOACCESS_CONF \
--output $GOACCESS_REPORT_HTML \
--output $GOACCESS_REPORT_JSON
else
  # create GOACCESS_DB_PATH if it does not exist
  mkdir -p $GOACCESS_DB_PATH
  # run goaccess with some defaults if no particular goaccess.conf is configured
  goaccess $GOACCESS_INPUT_FILES \
--log-format $GOACCESS_ACCESSLOG_FORMAT \
--ignore-crawlers \
--keep-last $GOACCESS_KEEP_LAST \
--geoip-database $GOACCESS_DBIP \
--persist \
--restore \
--db-path $GOACCESS_DB_PATH \
--output $GOACCESS_REPORT_HTML \
--output $GOACCESS_REPORT_JSON
fi

# copy report.html to the default nginx directory
cp $GOACCESS_REPORT_HTML $NGINX_HTML_PATH/

