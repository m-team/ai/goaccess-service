# goaccess-service

goaccess-service is [goaccess](https://goaccess.io/) web log analyser packed in the docker container to generate the analysis report and to provide this report as a webpage via [nginx](https://www.nginx.com/) web server. The Docker image includes free IP geolocation database from [DB-IP](https://db-ip.com). A typical usage of the goaccess-service docker container is within a larger web service based on micro-services architecture and leveraging reverse-proxy.

## Getting started

In the simplest scenario, one starts docker image and mounts host directory with web access logs in `/var/log/srv-access`, e.g.

```bash
docker run -d --network host -v /host/directory/with_access_logs/:/var/log/srv-access/ mteamkit/goaccess-service
```
goaccess web log analyser will run at pre-defined time via crond service, generate `report.html` which is accessed at `http://localhost`

## Advanced usage

1. mount additional host directory in /opt/goaccess-etc, e.g.

```bash
docker run -d --network host \
-v /host/directory/goaccess-etc/:/opt/goaccess-etc/ \
-v /host/directory/with_access_logs/:/var/log/srv-access/ \
mteamkit/goaccess-service
```

this host directory may include:
* goaccess.cron : custom configuration to run cron job for goaccess
* goaccess-skim.sh : bash script to pre-process/skim access logs before processing them by goaccess
* goaccess.conf : custom configuration of goaccess web log analyser

OR also mount a goaccess output directory, `/opt/goaccess-out/` to get access to goaccess outputs:

```bash
docker run -d --network host \
-v /host/directory/goaccess-etc/:/opt/goaccess-etc/ \
-v /host/directory/goaccess-out/:/opt/goaccess-out/ \
-v /host/directory/with_access_logs/:/var/log/srv-access/ \
mteamkit/goaccess-service
```

2. It is possible to redefine some internally used environment variables:
* `GOACCESS_ETC_PATH` - directory with configuration files for goaccess, default: `/opt/goaccess-etc`
* `GOACCESS_OUT_PATH` - directory to store goaccess outputs, default: `/opt/goaccess-out`
* `GOACCESS_WEB_ROUTE` - web route to access goaccess HTML report, default `goaccess`
* `GOACCESS_CRON` - path to custom cronjob configuration file, default: `$GOACCESS_ETC_PATH/goaccess.cron`
* `GOACCESS_EXEC` - main script to execute goaccess web log analyser, default: `/usr/local/bin/goaccess-exec.sh`
* `GOACCESS_CONF` - custom configuration file to use for goaccess, default location: `$GOACCESS_ETC_PATH/goaccess.conf`
* `GOACCESS_ACCESSLOG_PATH` - directory to search for access logs, default: `/var/log/srv-access/`
* `GOACCESS_ACCESSLOG_PATTERN` - pattern to search for access logs, default: `access*`
* `GOACCESS_ACCESSLOG_N` - number of access.log files to process, default: 1
* `GOACCESS_SKIM_SCRIPT` - defines where skim script is located, default: `$GOACCESS_ETC_PATH/goaccess-skim.sh`
* `GOACCESS_SKIMMED_PATH` - path to skimmed/filtered files, default: `$GOACCESS_OUT_PATH/skimmed/`
* `NGINX_HTML_PATH` - default html location for nginx, default: `/usr/share/nginx/html`
* `GOACCESS_REPORT_HTML` - path for the final goaccess HTML report, default: `$GOACCESS_OUT_PATH/report.html`<br>
(N.B. goaccess HTML report is automatically copied to `$NGINX_HTML_PATH`)
* `GOACCESS_REPORT_JSON` - path for the final goaccess JSON report, default: `$GOACCESS_OUT_PATH/report.json`<br>
if `GOACCESS_CONF` is defined, the following variables are not taken into account:
* `GOACCESS_ACCESSLOG_FORMAT` - access.log format, default: `COMBINED` (full list of supported formats: https://goaccess.io/man#options) 
* `GOACCESS_KEEP_LAST` - for how many days keep in storage, default: 180
* `GOACCESS_DBIP` - Database for IP to COUNTRY (options: https://db-ip.com; http://dev.maxmind.com/geoip/geoip2/geolite2/), default: `/opt/dbip/dbip-country-lite.mmdb`
* `GOACCESS_DB_PATH` - goaccess DB for persistance, default: `$GOACCESS_OUT_PATH/db`


## Acknowledgment
<a href='https://db-ip.com'>IP Geolocation by DB-IP</a>

## License
Please, see the [LICENSE](LICENSE) file.

